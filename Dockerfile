FROM node:14



WORKDIR /app

COPY package.json .



COPY yarn.lock .

COPY . .

RUN yarn install
# Run on port 1337
EXPOSE 1337
CMD ["yarn","develop"]