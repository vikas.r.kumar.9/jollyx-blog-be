module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '4ff9a9bffdbd429fa87e57053f7f8a11'),
  },
});
